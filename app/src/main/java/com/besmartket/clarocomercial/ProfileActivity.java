package com.besmartket.clarocomercial;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.besmartket.clarocomercial.utils.UserInfo;
import com.github.siyamed.shapeimageview.CircularImageView;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;


public class ProfileActivity extends Activity {

    private CircularImageView circularImageView;
    private UserInfo userInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_profile);

        userInfo = new UserInfo(this);

        circularImageView = (CircularImageView)findViewById(R.id.circularPicture);

        String storedImage = userInfo.getEncodedImage();
        if (!storedImage.equals("")){
            byte[] decodedImage = Base64.decode(storedImage, Base64.DEFAULT);
            Bitmap image = BitmapFactory.decodeByteArray(decodedImage, 0, decodedImage.length);
            circularImageView.setImageBitmap(image);
        }

        Intent mIntent = getIntent();

        TextView nameText = (TextView)findViewById(R.id.nameText);
        nameText.setText(userInfo.getUsername());

        TextView mailText = (TextView)findViewById(R.id.mailText);
        mailText.setText(userInfo.getMail());

        TextView regionText = (TextView)findViewById(R.id.regionText);
        regionText.setText("Región: " + userInfo.getRegion());

        TextView cityText = (TextView)findViewById(R.id.cityText);
        cityText.setText("Ciudad: " + userInfo.getCiudad());

        TextView phoneText = (TextView)findViewById(R.id.phoneText);
        phoneText.setText("Tel Fijo: " + userInfo.getTelefono());

        TextView celText = (TextView)findViewById(R.id.mobileText);
        celText.setText("Celular: " + userInfo.getCelular());

        ImageButton logout = (ImageButton)findViewById(R.id.logoutBtn);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userInfo.logout();
                Intent intent = new Intent(ProfileActivity.this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });

        ImageButton next = (ImageButton)findViewById(R.id.nextButton);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileActivity.this, MenuActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });

        if (mIntent.getBooleanExtra("disable_next", false)){
            next.setEnabled(false);
            next.setImageDrawable(null);
        }

        Button changeImage = (Button)findViewById(R.id.changePic);
        changeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, 300);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK){
            InputStream imageStream;
            try {
                imageStream = getContentResolver().openInputStream(data.getData());
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 2;
                Bitmap image = BitmapFactory.decodeStream(imageStream, null, options);
                circularImageView.setImageBitmap(image);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                image.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                byte[] byteArray = stream.toByteArray();

                userInfo.setEncodedImage(Base64.encodeToString(byteArray, Base64.DEFAULT));
            } catch (FileNotFoundException e) {
                Toast.makeText(this, "El archivo no fue encontrado. Por favor intente de nuevo.", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed(){
//        Intent intent = new Intent(ProfileActivity.this, LoginActivity.class);
//        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        startActivity(intent);
        finishAffinity();
    }
}
