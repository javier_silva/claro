package com.besmartket.clarocomercial.reportsummary;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Toast;

import com.besmartket.clarocomercial.MenuActivity;
import com.besmartket.clarocomercial.R;
import com.besmartket.clarocomercial.SideMenuActivity;
import com.besmartket.clarocomercial.comm.ServiceDelegate;
import com.besmartket.clarocomercial.comm.ServiceID;
import com.besmartket.clarocomercial.comm.ServiceParser;
import com.besmartket.clarocomercial.reports.FinishedReportActivity;
import com.besmartket.clarocomercial.reports.ReportInfo;
import com.besmartket.clarocomercial.utils.UserInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;


public class ReportSummaryActivity extends Activity implements ServiceDelegate{

    private Context context;
    private int nationalReport;
    private int brigadaReport;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_report_summary);

        context = this;

        ImageButton home = (ImageButton)findViewById(R.id.homeButton);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog alertDialog = new AlertDialog.Builder(context)
                        .setTitle("Atención")
                        .setMessage("Perderá el progreso del reporte al retornar al menu principal. Desea continuar?")
                        .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(context, MenuActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                finish();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //do nothing
                            }
                        })
                        .create();
                alertDialog.show();
            }
        });

        ImageButton menu = (ImageButton)findViewById(R.id.menuButton);
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent animActivity = new Intent(getApplicationContext(), SideMenuActivity.class);
                startActivity(animActivity);

                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });

        ImageButton backButton = (ImageButton)findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Calendar calendar = Calendar.getInstance();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        ReportInfo.date = dateFormat.format(calendar.getTime());

        ArrayList<ReportSummaryItem> reportSummaryItems = new ArrayList<>();

        ReportSummaryItem item = new ReportSummaryItem("DEPARTAMENTO - CIUDAD", ReportInfo.ciudad);
        reportSummaryItems.add(item);

        item = new ReportSummaryItem("UBICACIÓN", ReportInfo.direccion);
        reportSummaryItems.add(item);

        item = new ReportSummaryItem("COMPETENCIA", ReportInfo.competencia);
        reportSummaryItems.add(item);

        item = new ReportSummaryItem("OPERADOR/FABRICANTE", ReportInfo.empresa);
        reportSummaryItems.add(item);

        item = new ReportSummaryItem("CANAL", ReportInfo.canalVenta);
        reportSummaryItems.add(item);

        item = new ReportSummaryItem("TIPO DE REPORTE", ReportInfo.reportType);
        reportSummaryItems.add(item);

        if (!ReportInfo.segmentacion.equals("")){
            item = new ReportSummaryItem("SEGMENTACIÓN", ReportInfo.segmentacion);
            reportSummaryItems.add(item);
        }

        if (!ReportInfo.superficie.equals("")){
            item = new ReportSummaryItem("SUPERFICIE", ReportInfo.superficie);
            reportSummaryItems.add(item);
        }

        if (ReportInfo.opciones.size() > 0){
            item = new ReportSummaryItem("OPCIONES", ReportInfo.opciones);
            reportSummaryItems.add(item);
        }

        if (!ReportInfo.remarks.equals("")){
            item = new ReportSummaryItem("OBSERVACIONES", ReportInfo.remarks);
            reportSummaryItems.add(item);
        }

        if (!ReportInfo.comparativoClaro.equals("")){
            item = new ReportSummaryItem("COMPARATIVO CLARO", ReportInfo.comparativoClaro);
            reportSummaryItems.add(item);
        }

        ListView listView = (ListView)findViewById(R.id.mListView);
        listView.setAdapter(new ReportSummaryListAdapter(this, reportSummaryItems));

        RadioButton yesButton = (RadioButton)findViewById(R.id.yesRadioButton);
        yesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nationalReport = 1;
            }
        });

        RadioButton noButton = (RadioButton)findViewById(R.id.noRadioButton);
        noButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nationalReport = 0;
            }
        });

        RadioButton yesBrigadaButton = (RadioButton)findViewById(R.id.yesRadioButton2);
        yesBrigadaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                brigadaReport = 1;
            }
        });

        RadioButton noBrigadaButton = (RadioButton)findViewById(R.id.noRadioButton2);
        noBrigadaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                brigadaReport = 0;
            }
        });

        ImageButton nextButton = (ImageButton)findViewById(R.id.nextButton);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    sendReport();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    void sendReport() throws UnsupportedEncodingException {
        UserInfo userInfo = new UserInfo(this);
        String rawBody = "name=" + userInfo.getUserID()
                + "&regional=" + ReportInfo.regionId
                + "&gerencia=" + ReportInfo.gerenciaId
                + "&departamentos=" + ReportInfo.departmentId
                + "&ciudad=" + ReportInfo.cityId
                + "&fecha=" + ReportInfo.date
                + "&geo=" + ReportInfo.direccion.replace("'", "")
                + "&geoposicion=" + ReportInfo.geoposition
                + "&operador=" + ReportInfo.competenciaId
                + "&canal=" + ReportInfo.canalId
                + (!ReportInfo.superficieId.isEmpty() ? "&rep_adic=" + ReportInfo.superficieId : "")
                + "&tipo=" + ReportInfo.reportTypeId
                + "&nivel=" + ReportInfo.reportLevel
                + (!ReportInfo.segmentacion.isEmpty() ? "&segmento=" + ReportInfo.segmentacion : "")
                + "&imagen=" + ReportInfo.image
                + "&observaciones=" + ReportInfo.remarks.replace("%", " porciento").replace("'", "")
                + "&comparativo=" + ReportInfo.comparativoClaro.replace("%", " porciento").replace("'", "")
                + "&nacional=" + String.valueOf(nationalReport)
                + "&brigada=" + String.valueOf(brigadaReport);

        Log.i("Body", rawBody);

        ServiceParser parser = new ServiceParser(ServiceID.SEND_REPORT, rawBody, this, this);
        parser.execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_report_summary, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void serviceResponse(Object result) {
        if (result != null){
            String total;
            JSONArray jsonArray = (JSONArray)result;
            for (int i = 0;i < jsonArray.length();i++) {
                try {
                    final JSONObject regionObject = jsonArray.getJSONObject(i);

                    String repeated = regionObject.getString("repetido");

                    if (repeated.equals("0")){
                        total = regionObject.getString("total");

                        Intent intent = new Intent(getApplication(), FinishedReportActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("total", total);
                        startActivity(intent);
                    } else {
                        AlertDialog alertDialog = new AlertDialog.Builder(context)
                                .setTitle("Atención")
                                .setMessage("Ya existe un reporte con esta información.")
                                .setCancelable(false)
                                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent intent = new Intent(getApplication(), MenuActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);
                                        finish();
                                    }
                                })
                                .create();
                        alertDialog.show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(this, "Hubo un problema de comunicación. Por favor revise su conexión a Internet e intente de nuevo.", Toast.LENGTH_LONG).show();
                }
            }

        } else {
            Toast.makeText(this, "Hubo un problema de comunicación. Por favor revise su conexión a Internet e intente de nuevo.", Toast.LENGTH_LONG).show();
        }
    }
}
