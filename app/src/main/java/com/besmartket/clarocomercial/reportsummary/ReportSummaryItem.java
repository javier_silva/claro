package com.besmartket.clarocomercial.reportsummary;

import java.util.ArrayList;
import java.util.Set;

/**
 * Created by Trink2 on 7/17/2015.
 */
public class ReportSummaryItem {

    private String title;
    private String item;
    private Set<String> options;

    public ReportSummaryItem(String title, String item){
        this.title = title;
        this.item = item;
    }

    public ReportSummaryItem(String title, Set<String> options){
        this.title = title;
        this.options = options;
    }

    public String getTitle(){
        return title;
    }

    public String getItem(){
        return item;
    }

    public Set<String> getOptions() { return options; }

}
