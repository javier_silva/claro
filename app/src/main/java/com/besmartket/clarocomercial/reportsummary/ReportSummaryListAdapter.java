package com.besmartket.clarocomercial.reportsummary;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.besmartket.clarocomercial.R;

import java.util.List;

/**
 * Created by Trink2 on 7/17/2015.
 */
public class ReportSummaryListAdapter extends BaseAdapter {

    private Context context;
    private List<ReportSummaryItem> reportSummaryItems;

    public ReportSummaryListAdapter(Context context,List<ReportSummaryItem> reportSummaryItems){
        this.context = context;
        this.reportSummaryItems = reportSummaryItems;
    }

    @Override
    public int getCount() {
        return reportSummaryItems.size();
    }

    @Override
    public Object getItem(int position) {
        return reportSummaryItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.layout_report_item, parent,
                    false);
        }

        TextView title = (TextView)convertView.findViewById(R.id.title);
        title.setText(reportSummaryItems.get(position).getTitle());

        TextView name = (TextView)convertView.findViewById(R.id.name);

        String reportItem = reportSummaryItems.get(position).getItem();

        if (reportItem == null){
            reportItem = TextUtils.join(", ", reportSummaryItems.get(position).getOptions());
        }
        name.setText(reportItem);

        return convertView;
    }
}
