package com.besmartket.clarocomercial;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.besmartket.clarocomercial.comm.ServiceDelegate;
import com.besmartket.clarocomercial.comm.ServiceID;
import com.besmartket.clarocomercial.comm.ServiceParser;
import com.besmartket.clarocomercial.utils.UserInfo;

import org.json.JSONException;
import org.json.JSONObject;


public class LoginActivity extends Activity implements ServiceDelegate{

    private EditText name;
    private EditText pass;
    private UserInfo userInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_login);

        userInfo = new UserInfo(this);
        if (userInfo.isLogged()){
            startActivity(new Intent(this, ProfileActivity.class));
//            finish();
        }

        name = (EditText)findViewById(R.id.nameText);
        pass = (EditText)findViewById(R.id.password);

        ImageButton enter = (ImageButton)findViewById(R.id.enterBtn);
        enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (fieldsValidation())
                    loginAttempt();
            }
        });

        Button changePasswrd = (Button)findViewById(R.id.changePassword);
        changePasswrd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (name.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(), "Para cambiar la contraseña es necesario ingresar el Nombre de Usuario.", Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent(LoginActivity.this, PasswordActivity.class);
                    intent.putExtra("name", name.getText().toString());
                    startActivity(intent);
                }
            }
        });
    }

    boolean fieldsValidation(){
        if (name.getText().toString().equals("") || pass.getText().toString().equals("")){
            Toast.makeText(this, "Asegúrese de completar los datos.", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            return true;
        }
    }

    void loginAttempt(){
        userInfo.setUserID(name.getText().toString());

        String rawBody = "name=" + name.getText().toString()
                + "&pass=" + pass.getText().toString();

        Log.i("Body", rawBody);

        ServiceParser parser = new ServiceParser(ServiceID.LOGIN, rawBody, this, this);
        parser.execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void serviceResponse(Object result) {
        if (result != null){
            JSONObject jsonObject = (JSONObject)result;
            try {
                userInfo.setUsername(jsonObject.getString("usu_nombre") + " " + jsonObject.getString("usu_apellido"));

                if (jsonObject.has("ciu_nombre")){
                    userInfo.setCiudad(jsonObject.getString("ciu_nombre"));
                } else {
                    userInfo.setCiudad(jsonObject.getString("usu_id_ciudad"));
                }

                userInfo.setMail(jsonObject.getString("usu_correo"));
                userInfo.setRegion(jsonObject.getString("usu_id_reg"));
                userInfo.setTelefono(jsonObject.getString("usu_telefono"));

                if (jsonObject.has("usu_celular")){
                    userInfo.setCelular(jsonObject.getString("usu_celular"));
                }

                Intent intent = new Intent(this, ProfileActivity.class);
                startActivity(intent);
//                finish();
            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(this, "Hubo un problema de comunicación. Por favor revise su conexión a Internet e intente de nuevo.", Toast.LENGTH_LONG).show();
            }

        } else {
            Toast.makeText(this, "Hubo un problema de comunicación. Por favor revise su información y su conexión a Internet e intente de nuevo.", Toast.LENGTH_LONG).show();
        }
    }
}
