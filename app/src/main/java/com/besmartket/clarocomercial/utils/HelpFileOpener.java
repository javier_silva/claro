package com.besmartket.clarocomercial.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.besmartket.clarocomercial.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Created by Trink2 on 7/23/2015.
 */
public class HelpFileOpener {

    public void openHelpFile(Context context){

        File file = createFileFromInputStream(context, context.getResources().openRawResource(R.raw.ayudaclaro3));
        if (file == null){
            Toast.makeText(context, "Lo sentimos, pero debe tener habilitado un método de almacenamiento externo para usar esta función.", Toast.LENGTH_LONG).show();
            return;
        }

        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(file), "application/pdf");
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_GRANT_READ_URI_PERMISSION);
        List<ResolveInfo> resolved = context.getPackageManager().queryIntentActivities(intent, 0);
        if(resolved != null && resolved.size() > 0)
        {
            context.startActivity(intent);
        }
        else
        {
            Toast.makeText(context,
                    "Debe instalar una App para la lectura de este PDF.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    private File createFileFromInputStream(Context context, InputStream inputStream) {

        try{
            File dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Claro");
            dir.mkdir();
            File f = new File(dir, "ayuda.pdf");
            if (f.exists()){
                return f;
            }

            FileOutputStream outputStream = new FileOutputStream(f);
            byte buffer[] = new byte[1024];
            int length = 0;

            while((length=inputStream.read(buffer)) > 0) {
                outputStream.write(buffer,0,length);
            }

            outputStream.close();
            inputStream.close();

            return f;

        }catch (IOException e) {
            Log.e("file error", e.getLocalizedMessage());
            //Logging exception
        }

        return null;
    }

}
