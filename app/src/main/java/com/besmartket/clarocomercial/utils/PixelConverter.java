package com.besmartket.clarocomercial.utils;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.view.WindowManager;

/**
 * Created by Trink2 on 7/15/2015.
 */
public class PixelConverter {

    public static int dpValue(Context context, int pixels){
        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);

        float logicalDensity = displayMetrics.density;

        int dp = (int)Math.ceil(logicalDensity/pixels);

        return dp;
    }

    public static int convertPixelsToDp(float px, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        int dp = (int) Math.ceil(px / (metrics.densityDpi / 160f));
        return dp;
    }

}
