package com.besmartket.clarocomercial.utils;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import com.besmartket.clarocomercial.utils.PixelConverter;

/**
 * Created by Trink2 on 7/17/2015.
 */
public class CustomRadioButton {

    public static RadioButton newCustomRadioButton(Context context){
        RadioButton radioButton = new RadioButton(context);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(getDpValue(50, context), 0, getDpValue(50, context), 0);
        radioButton.setLayoutParams(layoutParams);
        radioButton.setGravity(Gravity.END);
        radioButton.setPadding(getDpValue(5, context), 0, getDpValue(25, context), 0);
        radioButton.setBackgroundColor(Color.WHITE);
        radioButton.setVisibility(View.VISIBLE);

        return radioButton;
    }

    static int getDpValue(float pixels, Context context){
        return PixelConverter.convertPixelsToDp(pixels, context);
    }
}
