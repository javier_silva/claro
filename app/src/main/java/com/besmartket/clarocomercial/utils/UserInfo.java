package com.besmartket.clarocomercial.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Trink2 on 2/18/2016.
 */
public class UserInfo {

    private static final String USERNAME_KEY = "username";
    private static final String REGION_KEY = "region";
    private static final String CIUDAD_KEY = "ciudad";
    private static final String MAIL_KEY = "mail";
    private static final String TELEFONO_KEY = "telefono";
    private static final String CELULAR_KEY = "celular";
    private static final String IMAGE_KEY = "profileImage";
    private static final String USER_ID_KEY = "userID";

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    public UserInfo(Context context){
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void setUsername(String username){
        editor = preferences.edit();
        editor.putString(USERNAME_KEY, username);
        editor.apply();
    }

    public String getUsername(){
        return preferences.getString(USERNAME_KEY, "");
    }

    public void setRegion(String region){
        editor = preferences.edit();
        editor.putString(REGION_KEY, region);
        editor.apply();
    }

    public String getRegion(){
        return preferences.getString(REGION_KEY, "");
    }

    public void setCiudad(String ciudad){
        editor = preferences.edit();
        editor.putString(CIUDAD_KEY, ciudad);
        editor.apply();
    }

    public String getCiudad(){
        return preferences.getString(CIUDAD_KEY, "");
    }

    public void setMail(String mail){
        editor = preferences.edit();
        editor.putString(MAIL_KEY, mail);
        editor.apply();
    }

    public String getMail(){
        return preferences.getString(MAIL_KEY, "");
    }

    public void setTelefono(String telefono){
        editor = preferences.edit();
        editor.putString(TELEFONO_KEY, telefono);
        editor.apply();
    }

    public String getTelefono(){
        return preferences.getString(TELEFONO_KEY, "");
    }

    public void setCelular(String celular){
        editor = preferences.edit();
        editor.putString(CELULAR_KEY, celular);
        editor.apply();
    }

    public String getCelular(){
        return preferences.getString(CELULAR_KEY, "");
    }

    public void setEncodedImage(String encodedImage){
        editor = preferences.edit();
        editor.putString(IMAGE_KEY, encodedImage);
        editor.apply();
    }

    public String getEncodedImage(){
        return preferences.getString(IMAGE_KEY, "");
    }

    public boolean isLogged(){
        return !preferences.getString(USERNAME_KEY, "").isEmpty();
    }


    public void setUserID(String userID) {
        editor = preferences.edit();
        editor.putString(USER_ID_KEY, userID);
        editor.apply();
    }

    public String getUserID(){
        return preferences.getString(USER_ID_KEY, "");
    }

    public void logout() {
        editor = preferences.edit();
        editor.remove(USERNAME_KEY);
        editor.apply();
    }
}
