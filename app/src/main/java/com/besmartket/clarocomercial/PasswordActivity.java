package com.besmartket.clarocomercial;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.besmartket.clarocomercial.comm.ServiceDelegate;
import com.besmartket.clarocomercial.comm.ServiceID;
import com.besmartket.clarocomercial.comm.ServiceParser;

import org.json.JSONException;
import org.json.JSONObject;


public class PasswordActivity extends Activity implements ServiceDelegate{

    private String username;
    private EditText currentPassword;
    private EditText newPassword;
    private EditText confirmPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_password);

        username = getIntent().getStringExtra("name");

        currentPassword = (EditText)findViewById(R.id.currentPassword);
        newPassword = (EditText)findViewById(R.id.newPassword);
        confirmPassword = (EditText)findViewById(R.id.confirmPassword);

        Button changeButton = (Button)findViewById(R.id.changeBtn);
        changeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (areFieldsOk())
                    changeAttempt();
            }
        });
    }

    boolean areFieldsOk(){
        String currentPass = currentPassword.getText().toString();
        String newPass = newPassword.getText().toString();
        String confirmPass = confirmPassword.getText().toString();
        boolean ok = false;

        if (currentPass.equals("") || newPass.equals("") || confirmPass.equals("")) {
            Toast.makeText(this, "Asegúrese de completar los datos.", Toast.LENGTH_SHORT).show();
        } else if (newPass.contains(" ")) {
            Toast.makeText(this, "La contraseña no puede tener espacios en blanco.", Toast.LENGTH_SHORT).show();
        } else if (newPass.length() < 4){
            Toast.makeText(this, "La contraseña debe ser de al menos 4 caracteres.", Toast.LENGTH_SHORT).show();
        } else if (!newPass.equals(confirmPass)){
            Toast.makeText(this, "Los campos de nueva contraseña y de confirmación no coinciden.", Toast.LENGTH_SHORT).show();
        } else {
            ok = true;
        }
        return ok;
    }

    void changeAttempt(){
        String rawBody = "name=" + username
                + "&pass1=" + currentPassword.getText().toString()
                + "&pass2=" + newPassword.getText().toString();

        Log.i("Body", rawBody);

        ServiceParser parser = new ServiceParser(ServiceID.PASSWORD_CHANGE, rawBody, this, this);
        parser.execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_password, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void serviceResponse(Object result) {
        if (result != null){
            JSONObject jsonObject = (JSONObject)result;

            try {
                String update = jsonObject.getString("actualizo");
                if (update.equals("1")){
                    Toast.makeText(this, "Actualización de contraseña exitosa.", Toast.LENGTH_LONG).show();
                    finish();
                }

            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(this, "La contraseña nueva no puede ser igual a la actual.", Toast.LENGTH_LONG).show();
            }

        } else {
            Toast.makeText(this, "Hubo un problema de comunicación. Por favor revise su información y su conexión a Internet e intente de nuevo.", Toast.LENGTH_LONG).show();
        }
    }
}
