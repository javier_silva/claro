package com.besmartket.clarocomercial;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;

import com.besmartket.clarocomercial.ranking.RankingActivity;
import com.besmartket.clarocomercial.reports.MyReportsActivity;
import com.besmartket.clarocomercial.utils.HelpFileOpener;
import com.besmartket.clarocomercial.utils.UserInfo;


public class SideMenuActivity extends Activity {

    private Context context;
    private String currentActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_side_menu);

        currentActivity = getIntent().getStringExtra("activity");

        context = this;

        ImageButton menu = (ImageButton)findViewById(R.id.menuButton);
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            }
        });

        ImageButton profile = (ImageButton)findViewById(R.id.profileButton);
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent activity = new Intent(getApplicationContext(),ProfileActivity.class);
                activity.putExtra("disable_next", true);
                startActivity(activity);

                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            }
        });

        ImageButton reports = (ImageButton)findViewById(R.id.reportsButton);
        reports.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!currentActivity.equals("myReports")){
                    Intent activity = new Intent(getApplicationContext(),MyReportsActivity.class);
                    startActivity(activity);
                }

                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            }
        });

        ImageButton ranking = (ImageButton)findViewById(R.id.rankingButton);
        ranking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!currentActivity.equals("ranking")){
                    Intent activity = new Intent(getApplicationContext(),RankingActivity.class);
                    startActivity(activity);
                }

                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            }
        });

        ImageButton help = (ImageButton)findViewById(R.id.helpButton);
        help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HelpFileOpener helpFileOpener = new HelpFileOpener();
                helpFileOpener.openHelpFile(context);
            }
        });

        final UserInfo userInfo = new UserInfo(this);

        ImageButton logout = (ImageButton)findViewById(R.id.logoutButton);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userInfo.logout();
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            }
        });
    }

    @Override
    public void onBackPressed(){
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_side_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
