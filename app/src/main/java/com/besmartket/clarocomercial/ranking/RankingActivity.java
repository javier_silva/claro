package com.besmartket.clarocomercial.ranking;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.besmartket.clarocomercial.R;
import com.besmartket.clarocomercial.SideMenuActivity;
import com.besmartket.clarocomercial.comm.ServiceDelegate;
import com.besmartket.clarocomercial.comm.ServiceID;
import com.besmartket.clarocomercial.comm.ServiceParser;
import com.besmartket.clarocomercial.utils.UserInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class RankingActivity extends Activity implements ServiceDelegate {

    private ArrayList<RankingItem> rankingItems;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_ranking);

        rankingItems = new ArrayList<>();
        listView = (ListView)findViewById(R.id.rankingListView);

        UserInfo userInfo = new UserInfo(this);
        String rawBody = "name=" + userInfo.getUserID();
        Log.i("Body", rawBody);

        ServiceParser parser = new ServiceParser(ServiceID.RANKING, rawBody, this, this);
        parser.execute();

        ImageButton back = (ImageButton)findViewById(R.id.backButton);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ImageButton home = (ImageButton)findViewById(R.id.homeButton);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ImageButton menu = (ImageButton)findViewById(R.id.menuButton);
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent animActivity = new Intent(getApplicationContext(),SideMenuActivity.class);
                animActivity.putExtra("activity", "ranking");
                startActivity(animActivity);

                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_ranking, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void serviceResponse(Object result) {
        int position = 0;
        if (result != null){
            JSONArray jsonArray = (JSONArray)result;
            for (int i = 0;i < jsonArray.length();i++){
                try {
                    final JSONObject regionObject = jsonArray.getJSONObject(i);
                    position++;
                    RankingItem rankingItem =
                            new RankingItem(String.valueOf(position),
                            regionObject.getString("usu_nombre") + " " + regionObject.getString("usu_apellido"),
                            regionObject.getString("total"));
                    rankingItems.add(rankingItem);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(this, "Hubo un problema de comunicación. Por favor revise su conexión a Internet e intente de nuevo.", Toast.LENGTH_LONG).show();
                }
            }

            listView.setAdapter(new RankingAdapter(this, rankingItems));

        } else {
            Toast.makeText(this, "Hubo un problema de comunicación. Por favor revise su conexión a Internet e intente de nuevo.", Toast.LENGTH_LONG).show();
        }
    }
}
