package com.besmartket.clarocomercial.ranking;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.besmartket.clarocomercial.R;

import java.util.ArrayList;

/**
 * Created by Trink2 on 7/18/2015.
 */
public class RankingAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<RankingItem> rankingItems;

    public RankingAdapter(Context context, ArrayList<RankingItem> rankingItems){
        this.context = context;
        this.rankingItems = rankingItems;
    }

    @Override
    public int getCount() {
        return rankingItems.size();
    }

    @Override
    public Object getItem(int position) {
        return rankingItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.layout_ranking_item, parent,
                    false);
        }

        TextView mPosition = (TextView)convertView.findViewById(R.id.position);
        mPosition.setText(rankingItems.get(position).getPosition());

        TextView nombre = (TextView)convertView.findViewById(R.id.name);
        nombre.setText(rankingItems.get(position).getName());

        TextView total = (TextView)convertView.findViewById(R.id.reports);
        total.setText(rankingItems.get(position).getReports());

        return convertView;
    }
}
