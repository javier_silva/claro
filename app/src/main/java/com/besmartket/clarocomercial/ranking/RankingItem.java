package com.besmartket.clarocomercial.ranking;

/**
 * Created by Trink2 on 7/18/2015.
 */
public class RankingItem {

    private String position;
    private String name;
    private String reports;

    public RankingItem(String position, String name, String reports){
        this.position = position;
        this.name = name;
        this.reports = reports;
    }

    public String getPosition(){ return position; }

    public String getName(){ return name; }

    public String getReports(){ return reports; }

}
