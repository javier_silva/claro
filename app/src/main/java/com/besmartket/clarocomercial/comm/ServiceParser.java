package com.besmartket.clarocomercial.comm;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

public class ServiceParser extends AsyncTask<Void, Void, Object> {
	
	String mParams;
	private ServiceDelegate delegate;
	private ServiceID mServiceID;
	ServerComm comm;
	ProgressDialog dialog;
	Context context;
	
	public ServiceParser(ServiceID serviceID, String rawBody, ServiceDelegate delegate, Context context){
		this.delegate = delegate;
		mParams = rawBody;
		this.mServiceID = serviceID;
		this.context = context;
		comm = new ServerComm();
	}
	
	@Override
	protected void onPreExecute() {
//		if (mServiceID != ServiceID.DATE) {
			dialog = new ProgressDialog(context);
			dialog.setTitle("Por favor espere");
			dialog.setMessage("Validando la información...");
			dialog.setCanceledOnTouchOutside(false);
			dialog.show();
//		}
		super.onPreExecute();
	}
		
	@Override
	protected Object doInBackground(Void... params) {
		String jsonStr = comm.getPOSTResult(mServiceID, mParams);
		Log.i("ServiceParser", jsonStr);
		Object jsonObj = null;
		try {
			jsonObj = new JSONArray(jsonStr);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			try {
				jsonObj = new JSONObject(jsonStr);
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		
		return jsonObj;
	}
	
	@Override
	protected void onPostExecute(Object result) {
		if (dialog != null) {
			dialog.dismiss();
		}
		if (delegate != null){
			delegate.serviceResponse(result);
		}
	}

}
