package com.besmartket.clarocomercial.comm;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.BaseAdapter;
import android.widget.ListView;

public class BitmapDecoder extends AsyncTask<Void, Void, byte[]> {
	
	ProgressDialog dialog;
	Context mContext;
	private String imageResource;
	private ListView listView;
	
	public BitmapDecoder(Context context, String urlResource, ListView listView){
		mContext = context;
		imageResource = urlResource;
		this.listView = listView;
	}
	
	@Override
	protected void onPreExecute() {
//			dialog = new ProgressDialog(mContext);
//			dialog.setTitle("Por favor espere");
//			dialog.setMessage("Validando la informaci�n...");
//			dialog.show();

		super.onPreExecute();
	}

	@Override
	protected byte[] doInBackground(Void... params) {
		InputStream input = null;
		HttpURLConnection connection = null;
	    try {
	        URL url = new URL(imageResource);
	        connection = (HttpURLConnection) url.openConnection();
	        connection.setDoInput(true);
	        connection.connect();
	        input = connection.getInputStream();
	        Bitmap myBitmap = BitmapFactory.decodeStream(input);
	        ByteArrayOutputStream bStream = new ByteArrayOutputStream();
			myBitmap.compress(Bitmap.CompressFormat.PNG, 100, bStream);
		    byte[] byteArray = bStream.toByteArray();
	        return byteArray;
	    } catch (Exception e) {
	        // Log exception
	        return null;
	    } finally {
	    	if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
	    	connection.disconnect();
	    }
	}
	
	@Override
	protected void onPostExecute(byte[] bitmap){
		//dialog.dismiss();
		if (bitmap != null){
			((BaseAdapter) listView.getAdapter()).notifyDataSetChanged();
		}
	}
	
}
