package com.besmartket.clarocomercial.comm;

import android.net.Uri;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

public class ServerComm {
	
	private static String serverURL = "http://pixostudio.com/claro/competencia/servicios.php/";

	public String getPOSTResult(ServiceID service, String mParams){
		String result ="";
		switch (service) {
			case LOGIN:
				result = postToService(serverURL + "login", mParams);
				break;
			case PASSWORD_CHANGE:
				result = postToService(serverURL + "cambio", mParams);
				break;
			case INFORM:
				result = postToService(serverURL + "mreportes", mParams);
				break;
			case RANKING:
				result = postToService(serverURL + "ranking", mParams);
				break;
			case GERENCIA:
				result = postToService(serverURL + "gerencias", mParams);
				break;
			case DEPARTMENTS:
				result = postToService(serverURL + "departamentos", mParams);
				break;
			case CITIES:
				result = postToService(serverURL + "ciudades", mParams);
				break;
			case REPORT:
				result = postToService(serverURL + "tipor", mParams);
				break;
			case CHANNEL_TYPE:
				result = postToService(serverURL + "tcanal", mParams);
				break;
			case ADDITIONAL:
				result = postToService(serverURL + "adicion", mParams);
				break;
			case REPORT_TYPE:
				result = postToService(serverURL + "tipo_repor", mParams);
				break;
			case CATEGORY:
				result = postToService(serverURL + "adic_dos", mParams);
				break;
			case SEND_REPORT:
				result = postToService(serverURL + "informe", mParams);
				break;
			case IMAGE_TEST:
				result = postToService(serverURL + "informe", mParams);
				break;
			case GET_REPORT:
				result = postToService(serverURL + "reportesp", mParams);
				break;
			default:
				break;
		}
		return result;
	}
	
	String postToService(String url, String mParams) {
		String serverResponse = "";
		try {
			HttpParams httpParameters = new BasicHttpParams();
			// Set the timeout in milliseconds until a connection is established.
			int timeoutConnection = 3000;
			HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);

			HttpClient httpClient = new DefaultHttpClient(httpParameters);
			HttpPost post = new HttpPost(url);
//			mParams = Uri.encode(mParams);
			post.setEntity(new StringEntity(mParams, "UTF-8"));
			post.setHeader("Content-type", "application/x-www-form-urlencoded");
//			post.setHeader("Accept", "application/json");
			HttpResponse response = httpClient.execute(post);
			HttpEntity entity = response.getEntity();
			serverResponse = EntityUtils.toString(entity);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return serverResponse;
	}
	
}
