package com.besmartket.clarocomercial.comm;

import org.json.JSONArray;

public interface ServiceDelegate {
	void serviceResponse(Object result);
}
