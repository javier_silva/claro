package com.besmartket.clarocomercial.reports;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Trink2 on 7/17/2015.
 */
public class ReportInfo {
    public static String competencia = "";
    public static String competenciaId = "";
    public static String empresa = "";
    public static String reportType = "";
    public static String reportTypeId = "";
    public static String ciudad = "";
    public static String direccion = "";
    public static String canalVenta = "";
    public static String canalId = "";
    public static String superficie = "";
    public static String superficieId = "";
    public static String segmentacion = "";
    public static Set<String> opciones = new HashSet<>();

    public static String gerenciaId = "";
    public static String reportLevel = "";
    public static String image = "";
    public static String regionId = "";
    public static String departmentId = "";
    public static String cityId = "";
    public static String date = "";
    public static String geoposition = "";
    public static String remarks = "";
    public static String comparativoClaro = "";

    public static void clearAll(){
        competencia = "";
        competenciaId = "";
        empresa = "";
        reportType = "";
        reportTypeId = "";
        ciudad = "";
        direccion = "";
        canalVenta = "";
        canalId = "";
        superficie = "";
        superficieId = "";
        segmentacion = "";
        opciones = new HashSet<>();

        gerenciaId = "";
        reportLevel = "";
        image = "";
        regionId = "";
        departmentId = "";
        cityId = "";
        date = "";
        geoposition = "";
        remarks = "";
        comparativoClaro = "";
    }
}
