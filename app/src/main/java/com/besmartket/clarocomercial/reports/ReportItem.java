package com.besmartket.clarocomercial.reports;

/**
 * Created by Trink2 on 7/18/2015.
 */
public class ReportItem {

    private String competencia;
    private String tipo;
    private String date;
    private String id;

    public ReportItem(String id, String competencia, String tipo, String date){
        this.id = id;
        this.competencia = competencia;
        this.tipo = tipo;
        this.date = date;
    }

    public String getId(){ return id; }

    public String getCompetencia(){return competencia;}

    public String getTipo(){return tipo;}

    public String getDate(){return date;}

}
