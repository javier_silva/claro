package com.besmartket.clarocomercial.reports;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.besmartket.clarocomercial.R;
import com.besmartket.clarocomercial.SideMenuActivity;
import com.besmartket.clarocomercial.comm.ServiceDelegate;
import com.besmartket.clarocomercial.comm.ServiceID;
import com.besmartket.clarocomercial.comm.ServiceParser;
import com.besmartket.clarocomercial.reports.ReportAdapter;
import com.besmartket.clarocomercial.reports.ReportItem;
import com.besmartket.clarocomercial.utils.UserInfo;
import com.github.siyamed.shapeimageview.CircularImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class MyReportsActivity extends Activity implements ServiceDelegate{

    private ArrayList<ReportItem> reportItems;
    private ListView listView;

    private TextView reportsTotal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_my_reports);

        UserInfo userInfo = new UserInfo(this);

        CircularImageView circularImageView = (CircularImageView) findViewById(R.id.circularPicture);
        String storedImage = userInfo.getEncodedImage();
        if (!storedImage.equals("")){
            byte[] decodedImage = Base64.decode(storedImage, Base64.DEFAULT);
            Bitmap image = BitmapFactory.decodeByteArray(decodedImage, 0, decodedImage.length);
            circularImageView.setImageBitmap(image);
        }

        reportItems = new ArrayList<>();
        listView = (ListView)findViewById(R.id.reportsListView);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String reportId = reportItems.get(i).getId();
                Intent intent = new Intent(getApplicationContext(), MyReportDetailsActivity.class);
                intent.putExtra("reportId", reportId);
                startActivity(intent);
            }
        });

        reportsTotal = (TextView)findViewById(R.id.reportText);

        String rawBody = "name=" + userInfo.getUserID();
        Log.i("Body", rawBody);

        ServiceParser parser = new ServiceParser(ServiceID.INFORM, rawBody, this, this);
        parser.execute();

        ImageButton back = (ImageButton)findViewById(R.id.backButton);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ImageButton home = (ImageButton)findViewById(R.id.homeButton);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        TextView nombre = (TextView)findViewById(R.id.nameText);
        nombre.setText(userInfo.getUsername());

        TextView region = (TextView)findViewById(R.id.regionText);
        region.setText("Region: " + userInfo.getRegion());

        ImageButton menu = (ImageButton)findViewById(R.id.menuButton);
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent animActivity = new Intent(getApplicationContext(), SideMenuActivity.class);
                animActivity.putExtra("activity", "myReports");
                startActivity(animActivity);

                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_my_reports, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void serviceResponse(Object result) {
        if (result != null){
            JSONArray jsonArray = (JSONArray)result;
            for (int i = 0;i < jsonArray.length();i++){
                try {
                    final JSONObject regionObject = jsonArray.getJSONObject(i);
                    ReportItem reportItem = new ReportItem(regionObject.getString("rep_id"), regionObject.getString("usu_nombre"), regionObject.getString("tip_nombre"), regionObject.getString("rep_fecha"));
                    reportItems.add(reportItem);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(this, "Hubo un problema de comunicación. Por favor revise su conexión a Internet e intente de nuevo.", Toast.LENGTH_LONG).show();
                }
            }

            reportsTotal.setText("Reportes: " + reportItems.size());

            listView.setAdapter(new ReportAdapter(this, reportItems));

        } else {
            Toast.makeText(this, "Hubo un problema de comunicación. Por favor revise su conexión a Internet e intente de nuevo.", Toast.LENGTH_LONG).show();
        }
    }
}
