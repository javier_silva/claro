package com.besmartket.clarocomercial.reports;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.besmartket.clarocomercial.R;
import com.besmartket.clarocomercial.SideMenuActivity;
import com.besmartket.clarocomercial.comm.ServiceDelegate;
import com.besmartket.clarocomercial.comm.ServiceID;
import com.besmartket.clarocomercial.comm.ServiceParser;
import com.besmartket.clarocomercial.reportsummary.ReportSummaryItem;
import com.besmartket.clarocomercial.reportsummary.ReportSummaryListAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

public class MyReportDetailsActivity extends Activity implements ServiceDelegate {

    private ArrayList<ReportSummaryItem> reportSummaryItems;
    private ListView listView;
    private ImageView reportImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_my_report_details);

        reportSummaryItems = new ArrayList<>();
        listView = (ListView)findViewById(R.id.reportListView);

        ImageButton back = (ImageButton)findViewById(R.id.backButton);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ImageButton home = (ImageButton)findViewById(R.id.homeButton);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ImageButton menu = (ImageButton)findViewById(R.id.menuButton);
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent animActivity = new Intent(getApplicationContext(), SideMenuActivity.class);
                animActivity.putExtra("activity", "myReports");
                startActivity(animActivity);

                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });

        View dialogImageLayout = getLayoutInflater().inflate(R.layout.layout_image_dialog, null);
        reportImage = (ImageView)dialogImageLayout.findViewById(R.id.image);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(dialogImageLayout);
        builder.setNegativeButton("Salir", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        final AlertDialog imageDialog = builder.create();

        Button showImage = (Button)findViewById(R.id.imageButton);
        showImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageDialog.show();
            }
        });

        String rawBody = "reporteid=" + getIntent().getStringExtra("reportId");
        Log.i("Body", rawBody);

        ServiceParser parser = new ServiceParser(ServiceID.GET_REPORT, rawBody, this, this);
        parser.execute();
    }

    @Override
    public void serviceResponse(Object result) {
        if (result != null){
            JSONArray jsonArray = (JSONArray)result;
            for (int i = 0;i < jsonArray.length();i++){
                try {
                    final JSONObject report = jsonArray.getJSONObject(i);
                    Iterator keys = report.keys();
                    while (keys.hasNext()) {
                        String key = (String) keys.next();
                        String value = (String) report.get(key);
                        if (key.equals("foto")){
                            byte[] decodedImage = Base64.decode(value, Base64.DEFAULT);
                            Bitmap imageBitmap = BitmapFactory.decodeByteArray(decodedImage, 0, decodedImage.length);
                            reportImage.setImageBitmap(imageBitmap);
                        } else if (key.equals("id")) {
                            //ignore it
                        } else {
                            if (key.equals("nacional") || key.equals("brigada")){
                                switch (value){
                                    case "0":
                                        value = "NO";
                                        break;
                                    case "1":
                                        value = "SI";
                                        break;
                                    default:
                                        break;
                                }
                            }
                            ReportSummaryItem item = new ReportSummaryItem(beautifyKeyName(key), value);
                            reportSummaryItems.add(item);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(this, "Hubo un problema de comunicación. Por favor revise su conexión a Internet e intente de nuevo.", Toast.LENGTH_LONG).show();
                }
            }

            listView.setAdapter(new ReportSummaryListAdapter(this, reportSummaryItems));

        } else {
            Toast.makeText(this, "Hubo un problema de comunicación. Por favor revise su conexión a Internet e intente de nuevo.", Toast.LENGTH_LONG).show();
        }
    }

    private String beautifyKeyName(String key){
        switch (key){
            case "nombre":
                return "Nombre";
            case "Geo":
                return "Dirección";
            case "canal":
                return "Canal";
            case "topfa":
                return "Operador/Fabricante";
            case "tipo":
                return "Tipo";
            case "adicion1":
                return "Adicional 1";
            case "adicion2":
                return "Adicional 2";
            case "nivel":
                return "Nivel";
            case "seg":
                return "Segmento";
            case "obs":
                return "Observaciones";
            case "comp":
                return "Competencia";
            case "nacional":
                return "Nacional";
            case "brigada":
                return "Zona Gris";
            default:
                return key;
        }
    }
}
