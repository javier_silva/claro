package com.besmartket.clarocomercial.reports;

/**
 * Created by Trink2 on 7/15/2015.
 */
public class Channel {

    private String id;
    private String name;
    private boolean flag;

    public Channel(String id, String name, boolean flag){
        this.id = id;
        this.name = name;
        this.flag = flag;
    }

    public String getId(){
        return id;
    }

    public String getName(){
        return name;
    }

    public boolean getFlag(){
        return flag;
    }
}
