package com.besmartket.clarocomercial.reports;

import android.app.Activity;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.besmartket.clarocomercial.R;
import com.besmartket.clarocomercial.comm.ServiceDelegate;
import com.besmartket.clarocomercial.comm.ServiceID;
import com.besmartket.clarocomercial.comm.ServiceParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link RegionFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link RegionFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RegionFragment extends android.support.v4.app.Fragment implements ServiceDelegate{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private int selectedRegion = 0;
    private String selectedGerencia = "";
    private SharedPreferences sharedPreferences;

    private RadioGroup radioGroup;

    private RadioButton radioButton1 = null;
    private RadioButton radioButton2 = null;

    private ImageButton region1Button = null;
    private ImageButton region2Button = null;
    private ImageButton region3Button = null;
    private ImageButton region4Button = null;
    private ImageButton region5Button = null;
    private ImageButton region6Button = null;

    private ArrayList<Gerencia> region1Gerencias;
    private ArrayList<Gerencia> region2Gerencias;
    private ArrayList<Gerencia> region3Gerencias;
    private ArrayList<Gerencia> region4Gerencias;
    private ArrayList<Gerencia> region5Gerencias;
    private ArrayList<Gerencia> region6Gerencias;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment RegionFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static RegionFragment newInstance(String param1, String param2) {
        RegionFragment fragment = new RegionFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public RegionFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_region, container, false);

        ReportInfo.regionId = "";
        ReportInfo.gerenciaId = "";

        region1Gerencias = new ArrayList<Gerencia>();
        region2Gerencias = new ArrayList<Gerencia>();
        region3Gerencias = new ArrayList<Gerencia>();
        region4Gerencias = new ArrayList<Gerencia>();
        region5Gerencias = new ArrayList<Gerencia>();
        region6Gerencias = new ArrayList<Gerencia>();

        for (int i = 1; i <= 6; i++){
            String rawBody = "region=" + String.valueOf(i);
            Log.i("Body", rawBody.toString());

            ServiceParser parser = new ServiceParser(ServiceID.GERENCIA, rawBody, this, getActivity());
            parser.execute();
        }


        sharedPreferences = getActivity().getSharedPreferences(getActivity().getApplication().getPackageName(), 0);

        radioGroup = (RadioGroup)rootView.findViewById(R.id.mRadioGroup);

        radioButton1 = (RadioButton)rootView.findViewById(R.id.radioButton);
        radioButton2 = (RadioButton)rootView.findViewById(R.id.radioButton2);

        region1Button = (ImageButton)rootView.findViewById(R.id.region1Button);
        region2Button = (ImageButton)rootView.findViewById(R.id.region2Button);
        region3Button = (ImageButton)rootView.findViewById(R.id.region3Button);
        region4Button = (ImageButton)rootView.findViewById(R.id.region4Button);
        region5Button = (ImageButton)rootView.findViewById(R.id.region5Button);
        region6Button = (ImageButton)rootView.findViewById(R.id.region6Button);

        region1Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                select1stRegion();
            }
        });

        region2Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                radioGroup.clearCheck();
                selectedRegion = 2;
                saveSelectedRegion(selectedRegion);

                region1Button.setImageDrawable(getResources().getDrawable(R.drawable.botons1));
                region2Button.setImageDrawable(getResources().getDrawable(R.drawable.botonseleccion2));
                region3Button.setImageDrawable(getResources().getDrawable(R.drawable.botons3));
                region4Button.setImageDrawable(getResources().getDrawable(R.drawable.botons4));
                region5Button.setImageDrawable(getResources().getDrawable(R.drawable.botons5));
                region6Button.setImageDrawable(getResources().getDrawable(R.drawable.botons6));

                Gerencia mGerencia = region2Gerencias.get(0);
                radioButton1.setText(mGerencia.getGerenciaName());
                mGerencia = region2Gerencias.get(1);
                radioButton2.setVisibility(View.VISIBLE);
                radioButton2.setText(mGerencia.getGerenciaName());
            }
        });

        region3Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                radioGroup.clearCheck();
                selectedRegion = 3;
                saveSelectedRegion(selectedRegion);

                region1Button.setImageDrawable(getResources().getDrawable(R.drawable.botons1));
                region2Button.setImageDrawable(getResources().getDrawable(R.drawable.botons2));
                region3Button.setImageDrawable(getResources().getDrawable(R.drawable.botonseleccion3));
                region4Button.setImageDrawable(getResources().getDrawable(R.drawable.botons4));
                region5Button.setImageDrawable(getResources().getDrawable(R.drawable.botons5));
                region6Button.setImageDrawable(getResources().getDrawable(R.drawable.botons6));

                Gerencia mGerencia = region3Gerencias.get(0);
                radioButton1.setText(mGerencia.getGerenciaName());
                mGerencia = region3Gerencias.get(1);
                radioButton2.setVisibility(View.VISIBLE);
                radioButton2.setText(mGerencia.getGerenciaName());
            }
        });

        region4Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                radioGroup.clearCheck();
                selectedRegion = 4;
                saveSelectedRegion(selectedRegion);

                region1Button.setImageDrawable(getResources().getDrawable(R.drawable.botons1));
                region2Button.setImageDrawable(getResources().getDrawable(R.drawable.botons2));
                region3Button.setImageDrawable(getResources().getDrawable(R.drawable.botons3));
                region4Button.setImageDrawable(getResources().getDrawable(R.drawable.botonseleccion4));
                region5Button.setImageDrawable(getResources().getDrawable(R.drawable.botons5));
                region6Button.setImageDrawable(getResources().getDrawable(R.drawable.botons6));

                Gerencia mGerencia = region4Gerencias.get(0);
                radioButton1.setText(mGerencia.getGerenciaName());

                if (region4Gerencias.size() >= 2){
                    mGerencia = region4Gerencias.get(1);
                    radioButton2.setVisibility(View.VISIBLE);
                    radioButton2.setText(mGerencia.getGerenciaName());
                } else {
                    radioButton2.setVisibility(View.GONE);
                }
            }
        });

        region5Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                radioGroup.clearCheck();
                selectedRegion = 5;
                saveSelectedRegion(selectedRegion);

                region1Button.setImageDrawable(getResources().getDrawable(R.drawable.botons1));
                region2Button.setImageDrawable(getResources().getDrawable(R.drawable.botons2));
                region3Button.setImageDrawable(getResources().getDrawable(R.drawable.botons3));
                region4Button.setImageDrawable(getResources().getDrawable(R.drawable.botons4));
                region5Button.setImageDrawable(getResources().getDrawable(R.drawable.botonseleccion5));
                region6Button.setImageDrawable(getResources().getDrawable(R.drawable.botons6));

                Gerencia mGerencia = region5Gerencias.get(0);
                radioButton1.setText(mGerencia.getGerenciaName());
                mGerencia = region5Gerencias.get(1);
                radioButton2.setVisibility(View.VISIBLE);
                radioButton2.setText(mGerencia.getGerenciaName());
            }
        });

        region6Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                radioGroup.clearCheck();
                selectedRegion = 6;
                saveSelectedRegion(selectedRegion);

                region1Button.setImageDrawable(getResources().getDrawable(R.drawable.botons1));
                region2Button.setImageDrawable(getResources().getDrawable(R.drawable.botons2));
                region3Button.setImageDrawable(getResources().getDrawable(R.drawable.botons3));
                region4Button.setImageDrawable(getResources().getDrawable(R.drawable.botons4));
                region5Button.setImageDrawable(getResources().getDrawable(R.drawable.botons5));
                region6Button.setImageDrawable(getResources().getDrawable(R.drawable.botonseleccion6));

                Gerencia mGerencia = region6Gerencias.get(0);
                radioButton1.setText(mGerencia.getGerenciaName());

                if (region6Gerencias.size() >= 2){
                    mGerencia = region6Gerencias.get(1);
                    radioButton2.setVisibility(View.VISIBLE);
                    radioButton2.setText(mGerencia.getGerenciaName());
                } else {
                    radioButton2.setVisibility(View.GONE);
                }

            }
        });

        radioButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Log.i("radioButton", "checked 1");
                Gerencia gerencia;
                switch (selectedRegion){
                    case 1:
                        gerencia = region1Gerencias.get(0);
                        selectedGerencia = gerencia.getGerenciaID();
                        break;
                    case 2:
                        gerencia = region2Gerencias.get(0);
                        selectedGerencia = gerencia.getGerenciaID();
                        break;
                    case 3:
                        gerencia = region3Gerencias.get(0);
                        selectedGerencia = gerencia.getGerenciaID();
                        break;
                    case 4:
                        gerencia = region4Gerencias.get(0);
                        selectedGerencia = gerencia.getGerenciaID();
                        break;
                    case 5:
                        gerencia = region5Gerencias.get(0);
                        selectedGerencia = gerencia.getGerenciaID();
                        break;
                    case 6:
                        gerencia = region6Gerencias.get(0);
                        selectedGerencia = gerencia.getGerenciaID();
                        break;
                    default:
                        break;
                }

                saveSelectedGerencia(selectedGerencia);
            }
        });

        radioButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Log.i("radioButton", "checked 2");
                Gerencia gerencia;
                switch (selectedRegion){
                    case 1:
                        gerencia = region1Gerencias.get(1);
                        selectedGerencia = gerencia.getGerenciaID();
                        break;
                    case 2:
                        gerencia = region2Gerencias.get(1);
                        selectedGerencia = gerencia.getGerenciaID();
                        break;
                    case 3:
                        gerencia = region3Gerencias.get(1);
                        selectedGerencia = gerencia.getGerenciaID();
                        break;
                    case 4:
                        gerencia = region4Gerencias.get(1);
                        selectedGerencia = gerencia.getGerenciaID();
                        break;
                    case 5:
                        gerencia = region5Gerencias.get(1);
                        selectedGerencia = gerencia.getGerenciaID();
                        break;
                    case 6:
                        gerencia = region6Gerencias.get(1);
                        selectedGerencia = gerencia.getGerenciaID();
                        break;
                    default:
                        break;
                }
//                Log.i("savedGerencia", selectedGerencia);
                saveSelectedGerencia(selectedGerencia);
            }
        });

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
//                Log.i("radioGroup", "selected " + String.valueOf(checkedId));
            }
        });


        return rootView;
    }

    void select1stRegion(){

        radioGroup.clearCheck();
        selectedRegion = 1;
        saveSelectedRegion(selectedRegion);

        region1Button.setImageDrawable(getResources().getDrawable(R.drawable.botonseleccion1));
        region2Button.setImageDrawable(getResources().getDrawable(R.drawable.botons2));
        region3Button.setImageDrawable(getResources().getDrawable(R.drawable.botons3));
        region4Button.setImageDrawable(getResources().getDrawable(R.drawable.botons4));
        region5Button.setImageDrawable(getResources().getDrawable(R.drawable.botons5));
        region6Button.setImageDrawable(getResources().getDrawable(R.drawable.botons6));

        Gerencia mGerencia = region1Gerencias.get(0);
        radioButton1.setText(mGerencia.getGerenciaName());
        mGerencia = region1Gerencias.get(1);
        radioButton2.setVisibility(View.VISIBLE);
        radioButton2.setText(mGerencia.getGerenciaName());
    }

    void saveSelectedRegion(int selectedRegion){
        ReportInfo.regionId = String.valueOf(selectedRegion);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("selectedRegion", selectedRegion);
        editor.commit();
    }

    void saveSelectedGerencia(String selectedGerencia){
        ReportInfo.gerenciaId = selectedGerencia;
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("selectedGerencia", selectedGerencia);
        editor.commit();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
//        try {
//            mListener = (OnFragmentInteractionListener) activity;
//        } catch (ClassCastException e) {
//            throw new ClassCastException(activity.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void serviceResponse(Object result) {
        if (result != null){
            JSONArray jsonArray = (JSONArray)result;
            for (int i = 0;i < jsonArray.length();i++){
                try {
                    JSONObject regionObject = jsonArray.getJSONObject(i);
                    Gerencia gerencia = new Gerencia(regionObject.getString("ger_id"), regionObject.getString("ger_nombre"));
                    String gerenciaID = regionObject.getString("ger_reg_id");
                    if (gerenciaID.equals("1")){
                        region1Gerencias.add(gerencia);
                    } else if (gerenciaID.equals("2")){
                        region2Gerencias.add(gerencia);
                    } else if (gerenciaID.equals("3")){
                        region3Gerencias.add(gerencia);
                    } else if (gerenciaID.equals("4")){
                        region4Gerencias.add(gerencia);
                    } else if (gerenciaID.equals("5")){
                        region5Gerencias.add(gerencia);
                    } else if (gerenciaID.equals("6")){
                        region6Gerencias.add(gerencia);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "Hubo un problema de comunicación. Por favor revise su conexión a Internet e intente de nuevo.", Toast.LENGTH_LONG).show();
                }
                if (region1Gerencias.size() >= 2){
                    select1stRegion();
                }
            }

        } else {
            Toast.makeText(getActivity(), "Hubo un problema de comunicación. Por favor revise su conexión a Internet e intente de nuevo.", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
