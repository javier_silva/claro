package com.besmartket.clarocomercial.reports;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.besmartket.clarocomercial.R;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Trink2 on 7/18/2015.
 */
public class OptionsAdapter extends BaseAdapter {

    private ArrayList<MyObject> options;
    private Context context;
    private Set<String> selectedOptions;

    public OptionsAdapter(Context context, ArrayList<MyObject> options){
        this.context = context;
        this.options = options;
        selectedOptions = new HashSet<>();
    }

    @Override
    public int getCount() {
        return options.size();
    }

    @Override
    public Object getItem(int position) {
        return options.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.layout_options_item, parent,
                    false);

            viewHolder = new ViewHolder();
            viewHolder.checkBox = (CheckBox)convertView.findViewById(R.id.checkBox);

            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder)convertView.getTag();
        }

        viewHolder.checkBox.setText(options.get(position).getName());
        viewHolder.checkBox.setTag(position);
        viewHolder.checkBox.setChecked(options.get(position).selected());
        viewHolder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                int position = (Integer) buttonView.getTag();
                options.get(position).setSelected(buttonView.isChecked());

                if (buttonView.isChecked()) {
                    selectedOptions.add(buttonView.getText().toString());
                } else {
                    selectedOptions.remove(buttonView.getText().toString());
                }
//                Log.i("opciones", selectedOptions.toString());
                ReportInfo.opciones = selectedOptions;
            }
        });

        return convertView;
    }

    public static class ViewHolder{
        public CheckBox checkBox;
    }

}
