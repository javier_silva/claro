package com.besmartket.clarocomercial.reports;

import android.app.Activity;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.besmartket.clarocomercial.utils.CustomRadioButton;
import com.besmartket.clarocomercial.R;
import com.besmartket.clarocomercial.comm.ServiceDelegate;
import com.besmartket.clarocomercial.comm.ServiceID;
import com.besmartket.clarocomercial.comm.ServiceParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ReportTypeFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ReportTypeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ReportTypeFragment extends android.support.v4.app.Fragment implements ServiceDelegate {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private RadioGroup radioGroup;
    private SharedPreferences sharedPreferences;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ReportTypeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ReportTypeFragment newInstance(String param1, String param2) {
        ReportTypeFragment fragment = new ReportTypeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public ReportTypeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_report_type, container, false);

        ReportInfo.reportType = "";
        ReportInfo.reportLevel = "";

        radioGroup = (RadioGroup)rootView.findViewById(R.id.radioGroup);

        sharedPreferences = getActivity().getSharedPreferences(getActivity().getApplication().getPackageName(), 0);

        String rawBody = "tcanal=" + sharedPreferences.getString("selectedChannel", "");
        Log.i("Body", rawBody.toString());

        ServiceParser parser = new ServiceParser(ServiceID.REPORT_TYPE, rawBody, this, getActivity());
        parser.execute();

        final ImageButton reportLevel1 = (ImageButton)rootView.findViewById(R.id.imageButton4);
        final ImageButton reportLevel2 = (ImageButton)rootView.findViewById(R.id.imageButton5);
        final ImageButton reportLevel3 = (ImageButton)rootView.findViewById(R.id.imageButton6);

        reportLevel1.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                reportLevel1.setImageDrawable(getResources().getDrawable(R.drawable.botonseleccion1));
                reportLevel2.setImageDrawable(getResources().getDrawable(R.drawable.botons2));
                reportLevel3.setImageDrawable(getResources().getDrawable(R.drawable.botons3));

                ReportInfo.reportLevel = "1";
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("selectedReportLevel", "1");
                editor.commit();
            }
        });

        reportLevel2.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                reportLevel1.setImageDrawable(getResources().getDrawable(R.drawable.botons1));
                reportLevel2.setImageDrawable(getResources().getDrawable(R.drawable.botonseleccion2));
                reportLevel3.setImageDrawable(getResources().getDrawable(R.drawable.botons3));

                ReportInfo.reportLevel = "2";
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("selectedReportLevel", "2");
                editor.commit();
            }
        });

        reportLevel3.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                reportLevel1.setImageDrawable(getResources().getDrawable(R.drawable.botons1));
                reportLevel2.setImageDrawable(getResources().getDrawable(R.drawable.botons2));
                reportLevel3.setImageDrawable(getResources().getDrawable(R.drawable.botonseleccion3));

                ReportInfo.reportLevel = "3";
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("selectedReportLevel", "3");
                editor.commit();
            }
        });

        return rootView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
//        try {
//            mListener = (OnFragmentInteractionListener) activity;
//        } catch (ClassCastException e) {
//            throw new ClassCastException(activity.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void serviceResponse(Object result) {
        if (result != null){
            JSONArray jsonArray = (JSONArray)result;
            for (int i = 0;i < jsonArray.length();i++){
                try {
                    final JSONObject regionObject = jsonArray.getJSONObject(i);
                    final String reportTypeId = regionObject.getString("tip_id");
                    final String category = regionObject.getString("categoria");
                    final String bandera = regionObject.getString("bandera");
                    final RadioButton radioButton = CustomRadioButton.newCustomRadioButton(getActivity());
                    radioButton.setText(regionObject.getString("tip_nombre"));
                    radioButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ReportInfo.reportType = radioButton.getText().toString();
                            ReportInfo.reportTypeId = reportTypeId;
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString("selectedReportType", reportTypeId);
                            editor.putString("reportTypeCategory", category);
                            editor.commit();
                        }
                    });
                    radioGroup.addView(radioButton);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "Hubo un problema de comunicación. Por favor revise su conexión a Internet e intente de nuevo.", Toast.LENGTH_LONG).show();
                }
            }

        } else {
            Toast.makeText(getActivity(), "Hubo un problema de comunicación. Por favor revise su conexión a Internet e intente de nuevo.", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
