package com.besmartket.clarocomercial.reports;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.besmartket.clarocomercial.R;

import java.util.ArrayList;

/**
 * Created by Trink2 on 7/18/2015.
 */
public class ReportAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<ReportItem> reportItems;

    public ReportAdapter(Context context, ArrayList<ReportItem> reportItems){
        this.context = context;
        this.reportItems = reportItems;
    }

    @Override
    public int getCount() {
        return reportItems.size();
    }

    @Override
    public Object getItem(int position) {
        return reportItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.layout_my_report, parent,
                    false);
        }

        TextView competencia = (TextView)convertView.findViewById(R.id.competencia);
        competencia.setText(reportItems.get(position).getCompetencia());

        TextView nombre = (TextView)convertView.findViewById(R.id.nombre);
        nombre.setText(reportItems.get(position).getTipo());

        TextView fecha = (TextView)convertView.findViewById(R.id.fecha);
        fecha.setText(reportItems.get(position).getDate());

        return convertView;
    }
}
