package com.besmartket.clarocomercial.reports;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.Toast;

import com.besmartket.clarocomercial.R;
import com.besmartket.clarocomercial.SideMenuActivity;
import com.besmartket.clarocomercial.reportsummary.ReportSummaryActivity;


public class ReportActivity extends FragmentActivity {

    private android.support.v4.app.Fragment currentFragment;
    private FragmentManager fragmentManager;
    private SharedPreferences sharedPreferences;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_report);

        context = this;

        ImageButton home = (ImageButton)findViewById(R.id.homeButton);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog alertDialog = new AlertDialog.Builder(context)
                        .setTitle("Atención")
                        .setMessage("Perderá el progreso del reporte al retornar al menú principal. Desea continuar?")
                        .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //do nothing
                            }
                        })
                        .create();
                alertDialog.show();
            }
        });

        ImageButton menu = (ImageButton)findViewById(R.id.menuButton);
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent animActivity = new Intent(getApplicationContext(),SideMenuActivity.class);
                startActivity(animActivity);

                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });

        sharedPreferences = this.getSharedPreferences(getPackageName(), 0);

        currentFragment = new RegionFragment();

        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.frame_container, currentFragment).commit();

        ImageButton backButton = (ImageButton)findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goBack();
            }
        });

        ImageButton nextButton = (ImageButton)findViewById(R.id.nextButton);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (currentFragment.getClass().equals(RegionFragment.class)){
                    String selectedGerencia = ReportInfo.gerenciaId;
                    if (!selectedGerencia.equals("")){
                        currentFragment = new CityFragment();
                    } else {
                        Toast.makeText(getApplicationContext(),"Debe seleccionar una Gerencia antes de avanzar.", Toast.LENGTH_SHORT).show();
                    }

                } else if (currentFragment.getClass().equals(CityFragment.class)){
                     currentFragment = new MapsFragment();
                } else if (currentFragment.getClass().equals(MapsFragment.class)){

                    if (ReportInfo.direccion.equals("")){
                        Toast.makeText(getApplicationContext(),"Debe recopilar la Ubicación antes de avanzar.", Toast.LENGTH_SHORT).show();
                    } else {
                        currentFragment = new CompetenciaFragment();
                    }

                } else if (currentFragment.getClass().equals(CompetenciaFragment.class)){

                    if (ReportInfo.competenciaId.equals("") || ReportInfo.empresa.equals("")){
                        Toast.makeText(getApplicationContext(),"Debe seleccionar una Competencia y una Empresa antes de avanzar.", Toast.LENGTH_SHORT).show();
                    } else {
                        currentFragment = new ChannelFragment();
                    }

                } else if (currentFragment.getClass().equals(ChannelFragment.class)) {

                    if (ReportInfo.canalVenta.equals("")){
                        Toast.makeText(getApplicationContext(),"Debe seleccionar un Canal de Venta antes de avanzar.", Toast.LENGTH_SHORT).show();
                    } else {
                        String adFlag = sharedPreferences.getString("aditionalFlag", "");
                        if (adFlag.equals("1")) {
                            currentFragment = new SurfacesFragment();
                        } else {
                            currentFragment = new ReportTypeFragment();
                        }
                    }

                } else if (currentFragment.getClass().equals(SurfacesFragment.class)){

                    if (ReportInfo.superficie.equals("")){
                        Toast.makeText(getApplicationContext(),"Debe seleccionar una Superficie antes de avanzar.", Toast.LENGTH_SHORT).show();
                    } else {
                        currentFragment = new ReportTypeFragment();
                    }

                } else if (currentFragment.getClass().equals(ReportTypeFragment.class)) {

                    if (ReportInfo.reportType.equals("") || ReportInfo.reportLevel.equals("")){
                        Toast.makeText(getApplicationContext(),"Debe seleccionar un Tipo y Nivel de Reporte antes de avanzar.", Toast.LENGTH_SHORT).show();
                    } else {
                        String categoria = sharedPreferences.getString("reportTypeCategory", "");
                        if (categoria.equals("1")) {
                            currentFragment = new SegmentationFragment();
                        } else if (categoria.equals("2") || categoria.equals("3")) {
                            currentFragment = new OptionsFragment();
                        } else {
                            currentFragment = new PhotoFragment();
                        }
                    }

                } else if (currentFragment.getClass().equals(SegmentationFragment.class)) {

                    if (ReportInfo.segmentacion.equals("")) {
                        Toast.makeText(getApplicationContext(), "Debe seleccionar una Segmentación antes de avanzar.", Toast.LENGTH_SHORT).show();
                    } else {
                        currentFragment = new PhotoFragment();
                    }

                } else if (currentFragment.getClass().equals(OptionsFragment.class)){

                    if (ReportInfo.opciones.size() == 0){
                        Toast.makeText(getApplicationContext(), "Debe seleccionar al menos una Opción antes de avanzar.", Toast.LENGTH_SHORT).show();
                    } else {
                        currentFragment = new PhotoFragment();
                    }

                } else if (currentFragment.getClass().equals(PhotoFragment.class)){
                    if (ReportInfo.image.equals("")){
                        Toast.makeText(getApplicationContext(), "Debe tomar la Fotografía antes de avanzar.", Toast.LENGTH_SHORT).show();
                    } else {
                        currentFragment = new RemarksFragment();
                    }

                } else if (currentFragment.getClass().equals(RemarksFragment.class)) {
                    if (ReportInfo.remarks.equals("")){
                        Toast.makeText(getApplicationContext(), "Debe anotar sus observaciones antes de avanzar.", Toast.LENGTH_SHORT).show();
                    } else {
                        currentFragment = new ComparativoClaroFragment();
                    }

                } else {
                    if (ReportInfo.comparativoClaro.equals("")){
                        Toast.makeText(getApplicationContext(), "Debe anotar su comparativo antes de avanzar.", Toast.LENGTH_SHORT).show();
                    } else {
                        Intent intent = new Intent(getApplication(), ReportSummaryActivity.class);
                        startActivity(intent);
                    }
                }

                fragmentManager.beginTransaction()
                        .replace(R.id.frame_container, currentFragment).commit();


            }
        });


    }

    @Override
    public void onBackPressed(){
        goBack();
    }

    private void goBack(){

        if (currentFragment.getClass().equals(RegionFragment.class)) {
            currentFragment = null;
            super.onBackPressed();
        } else if (currentFragment.getClass().equals(CityFragment.class)) {
            currentFragment = new RegionFragment();
        } else if (currentFragment.getClass().equals(MapsFragment.class)) {
            currentFragment = new CityFragment();
        } else if (currentFragment.getClass().equals(CompetenciaFragment.class)) {
            currentFragment = new MapsFragment();
        } else if (currentFragment.getClass().equals(ChannelFragment.class)) {
            currentFragment = new CompetenciaFragment();
        } else if (currentFragment.getClass().equals(ReportTypeFragment.class)) {
            String adFlag = sharedPreferences.getString("aditionalFlag", "");
            if (adFlag.equals("1")){
                currentFragment = new SurfacesFragment();
            } else {
                currentFragment = new ChannelFragment();
            }
        } else if (currentFragment.getClass().equals(SurfacesFragment.class)){
            currentFragment = new ChannelFragment();
        } else if (currentFragment.getClass().equals(PhotoFragment.class)) {

            String categoria = sharedPreferences.getString("reportTypeCategory", "");
            if (categoria.equals("1")) {
                currentFragment = new SegmentationFragment();
            } else if (categoria.equals("2") || categoria.equals("3")) {
                currentFragment = new OptionsFragment();
            } else {
                currentFragment = new ReportTypeFragment();
            }
        } else if (currentFragment.getClass().equals(SegmentationFragment.class) || currentFragment.getClass().equals(OptionsFragment.class)){

            currentFragment = new ReportTypeFragment();

        } else if (currentFragment.getClass().equals(RemarksFragment.class)){

            currentFragment = new PhotoFragment();

        } else if (currentFragment.getClass().equals(ComparativoClaroFragment.class)){
            currentFragment = new RemarksFragment();
        }

        if (currentFragment != null){
            fragmentManager.beginTransaction()
                    .replace(R.id.frame_container, currentFragment).commit();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if (requestCode == MapsFragment.REQUEST_SETTINGS){
            currentFragment.onActivityResult(requestCode, resultCode, data);
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == MapsFragment.REQUEST_LOCATION){
            currentFragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_report, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
