package com.besmartket.clarocomercial.reports;

import android.app.Activity;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.besmartket.clarocomercial.R;
import com.besmartket.clarocomercial.comm.ServiceDelegate;
import com.besmartket.clarocomercial.comm.ServiceID;
import com.besmartket.clarocomercial.comm.ServiceParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CityFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CityFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CityFragment extends android.support.v4.app.Fragment implements ServiceDelegate{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private SharedPreferences sharedPreferences;

    private ArrayList<Departamento> departamentos;
    private ArrayList<String> departamentosNames;

    private ArrayList<Ciudad> ciudades;
    private ArrayList<String> ciudadesNames;

    private OnFragmentInteractionListener mListener;

    private Spinner depSpinner;
    private Spinner citySpinner;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CityFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CityFragment newInstance(String param1, String param2) {
        CityFragment fragment = new CityFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public CityFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_city, container, false);

        ReportInfo.departmentId = "";
        ReportInfo.cityId = "";
        ReportInfo.ciudad = "";

        departamentos = new ArrayList<>();
        departamentosNames = new ArrayList<>();
        ciudades = new ArrayList<>();
        ciudadesNames = new ArrayList<>();

        sharedPreferences = getActivity().getSharedPreferences(getActivity().getApplication().getPackageName(), 0);

        String rawBody = "gerencia=" + sharedPreferences.getString("selectedGerencia", "");
        Log.i("Body", rawBody);

        ServiceParser parser = new ServiceParser(ServiceID.DEPARTMENTS, rawBody, this, getActivity());
        parser.execute();

        depSpinner = (Spinner)rootView.findViewById(R.id.departmentSpinner);
        citySpinner = (Spinner)rootView.findViewById(R.id.citySpinner);

        depSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.i("selected item", parent.getItemAtPosition(position).toString());
                ciudades.clear();
                ciudadesNames.clear();
                String selectedDep = parent.getItemAtPosition(position).toString();
                for (Departamento departamento : departamentos){
                    if (selectedDep.equals(departamento.getName())){
                        ReportInfo.departmentId = departamento.getId();
                        requestCitiesForDepartment(departamento.getId());
                        break;
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                return;
            }
        });

        citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                for (Ciudad ciudad : ciudades){
                    if (parent.getItemAtPosition(position).toString().equals(ciudad.getName())){
                        ReportInfo.cityId = ciudad.getId();
                        break;
                    }
                }

                ReportInfo.ciudad = parent.getItemAtPosition(position).toString();
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("selectedCity", parent.getItemAtPosition(position).toString());
                editor.commit();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                return;
            }
        });

        return rootView;
    }

    void requestCitiesForDepartment(String depId){
        String rawBody = "departamento=" + depId;
        ServiceParser parser = new ServiceParser(ServiceID.CITIES, rawBody, this, getActivity());
        parser.execute();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
//        try {
//            mListener = (OnFragmentInteractionListener) activity;
//        } catch (ClassCastException e) {
//            throw new ClassCastException(activity.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void serviceResponse(Object result) {
        if (result != null){
            boolean gotDeps = false;
            JSONArray jsonArray = (JSONArray)result;
            for (int i = 0;i < jsonArray.length();i++){
                try {
                    JSONObject regionObject = jsonArray.getJSONObject(i);
                    if (regionObject.has("dep_id")){
                        gotDeps = true;
                        Departamento departamento = new Departamento(regionObject.getString("dep_id"), regionObject.getString("dep_nombre"));
                        departamentos.add(departamento);
                        departamentosNames.add(departamento.getName());
                    } else {
                        Ciudad ciudad = new Ciudad(regionObject.getString("ciu_id"), regionObject.getString("ciu_nombre"));
                        ciudades.add(ciudad);
                        ciudadesNames.add(ciudad.getName());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "Hubo un problema de comunicación. Por favor revise su conexión a Internet e intente de nuevo.", Toast.LENGTH_LONG).show();
                }
            }
            if (gotDeps){
                ArrayAdapter<String> depAdapter = new ArrayAdapter<String>(getActivity(),
                        android.R.layout.simple_spinner_item, departamentosNames);
                depSpinner.setAdapter(depAdapter);
            }

            if (ciudadesNames.size() > 0){
                ArrayAdapter<String> cityAdapter = new ArrayAdapter<String>(getActivity(),
                        android.R.layout.simple_spinner_item, ciudadesNames);
                citySpinner.setAdapter(cityAdapter);
            }

        } else {
            Toast.makeText(getActivity(), "Hubo un problema de comunicación. Por favor revise su conexión a Internet e intente de nuevo.", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
