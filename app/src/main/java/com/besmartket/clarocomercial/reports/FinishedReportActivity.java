package com.besmartket.clarocomercial.reports;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.TextView;

import com.besmartket.clarocomercial.MenuActivity;
import com.besmartket.clarocomercial.R;
import com.besmartket.clarocomercial.SideMenuActivity;
import com.github.siyamed.shapeimageview.CircularImageView;


public class FinishedReportActivity extends Activity {

    private SharedPreferences sharedPreferences;
    private CircularImageView circularImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_finished_report);

        circularImageView = (CircularImageView)findViewById(R.id.circularPicture);
        sharedPreferences = getSharedPreferences(getApplication().getPackageName(), 0);

        String storagedImage = sharedPreferences.getString("profileImage", "");
        if (!storagedImage.equals("")){
            byte[] decodedImage = Base64.decode(storagedImage, Base64.DEFAULT);
            Bitmap image = BitmapFactory.decodeByteArray(decodedImage, 0, decodedImage.length);
            circularImageView.setImageBitmap(image);
        }

        TextView name = (TextView)findViewById(R.id.nameText);
        name.setText(sharedPreferences.getString("nombre",""));

        TextView mail = (TextView)findViewById(R.id.mailText);
        mail.setText(sharedPreferences.getString("mail",""));

        TextView reportCount = (TextView)findViewById(R.id.reportCount);
        reportCount.setText(getIntent().getStringExtra("total"));

        ImageButton backButton = (ImageButton)findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadMenuAndResetStack();
            }
        });

        ImageButton nextButton = (ImageButton)findViewById(R.id.nextButton);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadMenuAndResetStack();
            }
        });

        ImageButton home = (ImageButton)findViewById(R.id.homeButton);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadMenuAndResetStack();
            }
        });

        ImageButton menu = (ImageButton)findViewById(R.id.menuButton);
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent animActivity = new Intent(getApplicationContext(), SideMenuActivity.class);
                startActivity(animActivity);

                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });
    }

    @Override
    public void onBackPressed(){
        loadMenuAndResetStack();
    }

    void loadMenuAndResetStack(){
        Intent intent = new Intent(getApplication(), MenuActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_finished_report, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
