package com.besmartket.clarocomercial.reports;

/**
 * Created by Trink2 on 7/14/2015.
 */
public class Ciudad {

    private String id;
    private String name;

    public Ciudad(String id, String name){
        this.id = id;
        this.name = name;
    }

    public String getId(){
        return id;
    }

    public String getName(){
        return name;
    }

}
