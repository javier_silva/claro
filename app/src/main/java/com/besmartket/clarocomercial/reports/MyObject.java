package com.besmartket.clarocomercial.reports;

/**
 * Created by Trink2 on 8/1/2015.
 */
public class MyObject {

    private String name;
    private boolean selected;

    public MyObject(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }

    public boolean selected(){
        return selected;
    }

    public void setSelected(boolean selected){
        this.selected = selected;
    }
}
