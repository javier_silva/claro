package com.besmartket.clarocomercial.reports;

/**
 * Created by Trink2 on 7/14/2015.
 */
public class Gerencia {

    private String gerenciaID;
    private String gerenciaName;

    public Gerencia(String id, String name){
        this.gerenciaID = id;
        this.gerenciaName = name;
    }

    public String getGerenciaID(){
        return this.gerenciaID;
    }

    public String getGerenciaName(){
        return this.gerenciaName;
    }
}
