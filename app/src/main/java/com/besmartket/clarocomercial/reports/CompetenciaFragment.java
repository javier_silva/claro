package com.besmartket.clarocomercial.reports;

import android.app.Activity;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.besmartket.clarocomercial.utils.CustomRadioButton;
import com.besmartket.clarocomercial.R;
import com.besmartket.clarocomercial.comm.ServiceDelegate;
import com.besmartket.clarocomercial.comm.ServiceID;
import com.besmartket.clarocomercial.comm.ServiceParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CompetenciaFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CompetenciaFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CompetenciaFragment extends android.support.v4.app.Fragment implements ServiceDelegate {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private RadioGroup radioGroup;

    private ArrayList<String> operators;
    private ArrayList<String> manufacturers;

    private ArrayList<String> operatorIds;
    private ArrayList<String> manufacturerIds;

    private SharedPreferences sharedPreferences;
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CompetenciaFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CompetenciaFragment newInstance(String param1, String param2) {
        CompetenciaFragment fragment = new CompetenciaFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public CompetenciaFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_rivals, container, false);

        ReportInfo.competenciaId = "";
        ReportInfo.empresa = "";

        sharedPreferences = getActivity().getSharedPreferences(getActivity().getApplication().getPackageName(), 0);

        operators = new ArrayList<>();
        manufacturers = new ArrayList<>();

        operatorIds = new ArrayList<>();
        manufacturerIds = new ArrayList<>();

        for (int i = 1;i <= 2;i++) {
            String rawBody = "trep=" + String.valueOf(i);
            Log.i("Body", rawBody.toString());

            ServiceParser parser = new ServiceParser(ServiceID.REPORT, rawBody, this, getActivity());
            parser.execute();
        }

        radioGroup = (RadioGroup)rootView.findViewById(R.id.mRadioGroup);

        final RadioButton operator = (RadioButton)rootView.findViewById(R.id.operatorRadioButton);
        final RadioButton manufacturer = (RadioButton)rootView.findViewById(R.id.manufacturerRadioButton);

//        operatorSelected();

        operator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radioGroup.removeAllViews();
                operatorSelected();
            }
        });

        manufacturer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                radioGroup.removeAllViews();
                ReportInfo.competencia = "Fabricante";

                for (int i = 0; i < manufacturers.size(); i++) {
                    final RadioButton radioButton = CustomRadioButton.newCustomRadioButton(getActivity());
                    radioButton.setText(manufacturers.get(i));
                    final int finalI = i;
                    radioButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ReportInfo.empresa = radioButton.getText().toString();
                            ReportInfo.competenciaId = manufacturerIds.get(finalI);
                        }
                    });

                    radioGroup.addView(radioButton);
                }

                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("selectedReportType", "2");
                editor.commit();

            }
        });

        return rootView;
    }

    void operatorSelected(){
        ReportInfo.competencia = "Operador";
        for (int j = 0;j < operators.size();j++){
//            RadioButton radioButton = (RadioButton) radioGroup.getChildAt(i);
            final RadioButton radioButton = CustomRadioButton.newCustomRadioButton(getActivity());
            radioButton.setText(operators.get(j));
            final int finalJ = j;
            radioButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ReportInfo.empresa = radioButton.getText().toString();
                    ReportInfo.competenciaId = operatorIds.get(finalJ);

                }
            });

            radioGroup.addView(radioButton);
        }

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("selectedReportType", "1");
        editor.commit();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
//        try {
//            mListener = (OnFragmentInteractionListener) activity;
//        } catch (ClassCastException e) {
//            throw new ClassCastException(activity.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void serviceResponse(Object result) {
        if (result != null){
            JSONArray jsonArray = (JSONArray)result;
            for (int i = 0;i < jsonArray.length();i++){
                try {
                    JSONObject regionObject = jsonArray.getJSONObject(i);
                    String reportType = regionObject.getString("tip_rep_id");

                    if (reportType.equals("1")){
                        operators.add(regionObject.getString("tip_nombre"));
                        operatorIds.add(regionObject.getString("tip_id"));
                    } else {
                        manufacturers.add(regionObject.getString("tip_nombre"));
                        manufacturerIds.add(regionObject.getString("tip_id"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "Hubo un problema de comunicación. Por favor revise su conexión a Internet e intente de nuevo.", Toast.LENGTH_LONG).show();
                }
            }

        } else {
            Toast.makeText(getActivity(), "Hubo un problema de comunicación. Por favor revise su conexión a Internet e intente de nuevo.", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
